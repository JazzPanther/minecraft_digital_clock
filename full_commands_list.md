# Full Commands List
## Scoreboard
```
/scoreboard objectives add m1 dummy
/scoreboard objectives add m10 dummy
/scoreboard objectives add h1 dummy
/scoreboard objectives add h10 dummy
/scoreboard objectives add gametick dummy
/scoreboard objectives add mcount dummy
```
## 7-segment Displays
### m1
#### segment A
```
/testfor @p[score_m1_min=1,score_m1=1]
/testfor @p[score_m1_min=4,score_m1=4]
```
#### segment B
```
/testfor @p[score_m1_min=5,score_m1=5]
/testfor @p[score_m1_min=6,score_m1=6]
```
#### segment C
```
/testfor @p[score_m1_min=2,score_m1=2]
```
#### segment D
```
/testfor @p[score_m1_min=1,score_m1=1]
/testfor @p[score_m1_min=4,score_m1=4]
/testfor @p[score_m1_min=7,score_m1=7]
```
#### segment E
```
/testfor @p[score_m1_min=1,score_m1=1]
/testfor @p[score_m1_min=3,score_m1=3]
/testfor @p[score_m1_min=4,score_m1=4]
/testfor @p[score_m1_min=5,score_m1=5]
/testfor @p[score_m1_min=7,score_m1=7]
/testfor @p[score_m1_min=9,score_m1=9]
```
#### segment F
```
/testfor @p[score_m1_min=1,score_m1=1]
/testfor @p[score_m1_min=2,score_m1=2]
/testfor @p[score_m1_min=3,score_m1=3]
/testfor @p[score_m1_min=7,score_m1=7]
```
#### segment G
```
/testfor @p[score_m1_min=0,score_m1=0]
/testfor @p[score_m1_min=1,score_m1=1]
/testfor @p[score_m1_min=7,score_m1=7]
```
### m10
#### segment A
```
/testfor @p[score_m10_min=1,score_m10=1]
/testfor @p[score_m10_min=4,score_m10=4]
```
#### segment B
```
/testfor @p[score_m10_min=5,score_m10=5]
/testfor @p[score_m10_min=6,score_m10=6]
```
#### segment C
```
/testfor @p[score_m10_min=2,score_m10=2]
```
#### segment D
```
/testfor @p[score_m10_min=1,score_m10=1]
/testfor @p[score_m10_min=4,score_m10=4]
/testfor @p[score_m10_min=7,score_m10=7]
```
#### segment E
```
/testfor @p[score_m10_min=1,score_m10=1]
/testfor @p[score_m10_min=3,score_m10=3]
/testfor @p[score_m10_min=4,score_m10=4]
/testfor @p[score_m10_min=5,score_m10=5]
/testfor @p[score_m10_min=7,score_m10=7]
/testfor @p[score_m10_min=9,score_m10=9]
```
#### segment F
```
/testfor @p[score_m10_min=1,score_m10=1]
/testfor @p[score_m10_min=2,score_m10=2]
/testfor @p[score_m10_min=3,score_m10=3]
/testfor @p[score_m10_min=7,score_m10=7]
```
#### segment G
```
/testfor @p[score_m10_min=0,score_m10=0]
/testfor @p[score_m10_min=1,score_m10=1]
/testfor @p[score_m10_min=7,score_m10=7]
```
### h1
#### segment A
```
/testfor @p[score_h1_min=1,score_h1=1]
/testfor @p[score_h1_min=4,score_h1=4]
```
#### segment B
```
/testfor @p[score_h1_min=5,score_h1=5]
/testfor @p[score_h1_min=6,score_h1=6]
```
#### segment C
```
/testfor @p[score_h1_min=2,score_h1=2]
```
#### segment D
```
/testfor @p[score_h1_min=1,score_h1=1]
/testfor @p[score_h1_min=4,score_h1=4]
/testfor @p[score_h1_min=7,score_h1=7]
```
#### segment E
```
/testfor @p[score_h1_min=1,score_h1=1]
/testfor @p[score_h1_min=3,score_h1=3]
/testfor @p[score_h1_min=4,score_h1=4]
/testfor @p[score_h1_min=5,score_h1=5]
/testfor @p[score_h1_min=7,score_h1=7]
/testfor @p[score_h1_min=9,score_h1=9]
```
#### segment F
```
/testfor @p[score_h1_min=1,score_h1=1]
/testfor @p[score_h1_min=2,score_h1=2]
/testfor @p[score_h1_min=3,score_h1=3]
/testfor @p[score_h1_min=7,score_h1=7]
```
#### segment G
```
/testfor @p[score_h1_min=0,score_h1=0]
/testfor @p[score_h1_min=1,score_h1=1]
/testfor @p[score_h1_min=7,score_h1=7]
```
### h10
#### segment A
```
/testfor @p[score_h10_min=1,score_h10=1]
/testfor @p[score_h10_min=4,score_h10=4]
```
#### segment B
```
/testfor @p[score_h10_min=5,score_h10=5]
/testfor @p[score_h10_min=6,score_h10=6]
```
#### segment C
```
/testfor @p[score_h10_min=2,score_h10=2]
```
#### segment D
```
/testfor @p[score_h10_min=1,score_h10=1]
/testfor @p[score_h10_min=4,score_h10=4]
/testfor @p[score_h10_min=7,score_h10=7]
```
#### segment E
```
/testfor @p[score_h10_min=1,score_h10=1]
/testfor @p[score_h10_min=3,score_h10=3]
/testfor @p[score_h10_min=4,score_h10=4]
/testfor @p[score_h10_min=5,score_h10=5]
/testfor @p[score_h10_min=7,score_h10=7]
/testfor @p[score_h10_min=9,score_h10=9]
```
#### segment F
```
/testfor @p[score_h10_min=1,score_h10=1]
/testfor @p[score_h10_min=2,score_h10=2]
/testfor @p[score_h10_min=3,score_h10=3]
/testfor @p[score_h10_min=7,score_h10=7]
```
#### segment G
```
/testfor @p[score_h10_min=0,score_h10=0]
/testfor @p[score_h10_min=1,score_h10=1]
/testfor @p[score_h10_min=7,score_h10=7]
```
## Carry-Over
### m1 to m10
```
/testfor @p[score_m1_min=10,score_m1=10]
```
```
/scoreboard players set @a m1 0
/scoreboard players add @a m10 1
```
### m10 to h1
```
/testfor @p[score_m10_min=6,score_m10=6]
```
```
/scoreboard players set @a m10 0
/scoreboard players add @a h1 1
```
### h1 to h10
```
/testfor @p[score_h1_min=10,score_h1=10]
```
```
/scoreboard players set @a h1 0
/scoreboard players add @a h10 1
```
### h10 to clock rollover (for 12hr)
```
/testfor @p[score_h10_min=1,score_h1=1]
/testfor @p[score_h1_min=3,score_h1=3]
```
```
/scoreboard players set @a h10 0
/scoreboard players set @a h1 1
/scoreboard players set @a m10 0
/scoreboard players set @a m1 0
```
### h10 to clock rollover (for 24hr)
```
/testfor @p[score_h10_min=2,score_h1=2]
/testfor @p[score_h1_min=4,score_h1=4]
```
```
/scoreboard players set @a h10 0
/scoreboard players set @a h1 0
/scoreboard players set @a m10 0
/scoreboard players set @a m1 0
```
## Main Control
### *gametick*
```
/scoreboard players add @a gametick 1
/testfor @p[score_gametick_min=17]
```
```
/scoreboard players set @a gametick 0
```
### *mcount*
```
/scoreboard players add @a mcount 1
/testfor @p[score_mcount_min=1197]
```
```
/scoreboard players set @a mcount 0
/scoreboard players add @a m1 1
```
## Blinking Colon
```
/testfor @p[score_gametick=7]
```
## Clock-Setting Controls
### Setting Time
```
/scoreboard players add @a m1 1
```
```
/scoreboard players add @a m10 1
```
```
/scoreboard players add @a h1 1
```
```
/scoreboard players add @a h10 1
```
### Reset
```
/scoreboard players set @a m1 0
/scoreboard players set @a m10 0
/scoreboard players set @a h1 0
/scoreboard players set @a h10 0
/scoreboard players set @a gametick 0
/scoreboard players set @a mcount 0
```
