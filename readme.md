# Digital Clock - Minecraft 1.12

## Scoreboard

For each of the following names, create all of the following objectives with `/scoreboard objectives add <NAME> dummy`:

`m1`: minutes 1's place digit - increments based on mcount, resets when =10

`m10`: minutes 10's place digit - increments when m1 resets, resets when =6

`h1`: hours 1's place digit - increments when m10 resets, resets when =2 for 12hr or =4 for 24hr

`h10`: hours 10's place digit - increments when h1 resets, resets when =1 for 12hr or =2 for 24hr

`gametick`: increments every game tick - resets when =17 to count seconds, used for blinking colon (can be excluded if not including colon)

`mcount`: increments every game tick - resets when =1197 to count minutes, used to increment m1

The game runs at 20 ticks per second (1200 ticks per minute). Accounting for redstone propagation delay, resets for `gametick` and `mcount` are triggered about 3 ticks early so the timing works.

## 7-Segment Displays

For the objective names `m1`, `m10`, `h1`, and `h10`, use the clock's truth table to create sets of command blocks for each segment with the following structure:

`/testfor @p[score_<NAME>_min=<DIGIT>,score_<NAME>=<DIGIT>]`

The digit value represents moments when the given segment is not visible for that number (in other words, the pistons are extended so the segment's blocks are flush with the display face).

For example, segment *C* is only ever turned off for the digit *2*; its set will contain one command block. If we're placing this set in segment *C* of display *m1*, the resulting single command block will contain `/testfor @p[score_m1_min=2,score_m1=2]`

All command blocks with these commands should have the properties of `Repeat` and `Always Active`. The result will be many sets of command blocks that operate independently to track the state of the display's current value, turning on or off their respective sets of pistons to emulate 7-segment display operation.

## Carry-Over

While the main control increments on *m1*, the carry-over increments on the other three displays. The first block follows the same format as the blocks used in the display segments:

`/testfor @p[score_<NAME>_min=<DIGIT>,score_<NAME>=<DIGIT>]`

The output will determine when to increment the next display. For example, testing *m10* for a digit value of *6* will trigger the following two blocks to reset *m10* and increment on *h1*:

`/scoreboard players set @a m10 0`

`/scoreboard players add @a h1 1`

The final carry-over will act as a clock rollover. The clock can return to its lowest displayed values by testing both *h10* and *h1* for their highest possible values. The values for this rollover can be changed depending on whether a 12-hour clock or a 24-hour clock is desired, i.e. when the time reaches *13:00*, set the clock to *01:00* and when the time reaches *24:00*, set the clock to *00:00*.

## Main Control

All values displayed on the clock are determined by the main control incrementing on tick-based values. The entry-point into the display system is when *mcount* has accumulated almost enough points equivalent to one minute worth of game ticks,

`/scoreboard players add @a mcount 1`

`/testfor @p[score_mcount_min=1197]`

 resetting itself and incrementing on *m1*:

`/scoreboard players set @a mcount 0`

`/scoreboard players add @a m1 1`

If a blinking colon is used, the value *gametick* can be used to keep track of one second worth of game ticks in almost the same manner, testing for a value of *17* and only resetting itself with no other incrementing.

## Blinking Colon

The colon can operate with a single-block output that tests *gametick* for a value roughly half of 1 second and accounting for redstone propagation delay:

`/testfor @p[score_gametick=7]`

## Setting and Reset

The clock can be set with commands that simply increment on the values of *m1*, *m10*, *h1*, and *h10*:

`/scoreboard players add @a m1 1`

`/scoreboard players add @a m10 1`

`/scoreboard players add @a h1 1`

`/scoreboard players add @a h10 1 `

The clock can be reset by setting all values to *0*:

`/scoreboard players set @a m1 0`

`/scoreboard players set @a m10 0`

`/scoreboard players set @a h1 0`

`/scoreboard players set @a h10 0`

`/scoreboard players set @a gametick 0`

`/scoreboard players set @a mcount 0`
